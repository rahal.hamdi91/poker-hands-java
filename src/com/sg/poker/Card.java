package com.sg.poker;


public class Card {
    private String value;
    private String type;

    private Integer note;

    public Card() {
    }

    public Card(String value, String type) {
        setValue(value);
        setType(type);
    }

    private void setNote(String value) {
        switch (value) {
            case ("T"):
                this.note = 10;
                break;
            case ("J"):
                this.note = 11;
                break;
            case ("Q"):
                this.note = 12;
                break;
            case ("K"):
                this.note = 13;
                break;
            case ("A"):
                this.note = 14;
                break;
            default : this.note = Integer.valueOf(this.value);
        }
    }

    public Integer getNote() {
        return this.note;
    }

    public void setValue(String v) {
        this.value = v;
        setNote(v);
    }

    public String getValue() {
        return value;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return value;
    }


}

