package com.sg.poker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        Card c1 = new Card("2","H");
        Card c2 = new Card("3","D");
        Card c3 = new Card("5","S");
        Card c4 = new Card("9","C");
        Card c5 = new Card("K","D");
        Hand h1 = new Hand();
        List<Card> l1 = new ArrayList();l1.add(c1);l1.add(c2);l1.add(c3);l1.add(c4);l1.add(c5);
        h1.setCards(l1);
        h1.setName("Black");

        Card c6 = new Card("2","C");
        Card c7 = new Card("3","H");
        Card c8 = new Card("4","S");
        Card c9 = new Card("8","C");
        Card c10 = new Card("A","H");
        Hand h2 = new Hand();
        List<Card> l2 = new ArrayList();l2.add(c6);l2.add(c7);l2.add(c8);l2.add(c9);l2.add(c10);
        h2.setCards(l2);
        h2.setName("White");

        System.out.println("Test 1 : "+h1+"   "+h2);
        System.out.println("===> "+h1.compareToOtherHand(h2));

        Card c11 = new Card("2","H");
        Card c12 = new Card("4","S");
        Card c13 = new Card("4","C");
        Card c14 = new Card("2","D");
        Card c15 = new Card("4","H");
        Hand h3 = new Hand();
        List<Card> l3 = new ArrayList();l3.add(c11);l3.add(c12);l3.add(c13);l3.add(c14);l3.add(c15);
        h3.setCards(l3);
        h3.setName("Black");

        Card c16 = new Card("2","S");
        Card c17 = new Card("8","S");
        Card c18 = new Card("A","S");
        Card c19 = new Card("Q","S");
        Card c20 = new Card("3","S");
        Hand h4 = new Hand();
        List<Card> l4 = new ArrayList();l4.add(c16);l4.add(c17);l4.add(c18);l4.add(c19);l4.add(c20);
        h4.setCards(l4);
        h4.setName("White");

        System.out.println("Test 2 : "+h3+"   "+h4);
        System.out.println("===> "+h3.compareToOtherHand(h4));

        Card c21 = new Card("2","H");
        Card c22 = new Card("3","D");
        Card c23 = new Card("5","S");
        Card c24 = new Card("9","C");
        Card c25 = new Card("K","D");
        Hand h5 = new Hand();
        List<Card> l5 = new ArrayList();l5.add(c21);l5.add(c22);l5.add(c23);l5.add(c24);l5.add(c25);
        h5.setCards(l5);
        h5.setName("Black");

        Card c26 = new Card("2","C");
        Card c27 = new Card("3","H");
        Card c28 = new Card("4","S");
        Card c29 = new Card("8","C");
        Card c30 = new Card("K","H");
        Hand h6 = new Hand();
        List<Card> l6 = new ArrayList();l6.add(c26);l6.add(c27);l6.add(c28);l6.add(c29);l6.add(c30);
        h6.setCards(l6);
        h6.setName("White");

        System.out.println("Test 3 : "+h5+"   "+h6);
        System.out.println("===> "+h5.compareToOtherHand(h6));

        Card c31 = new Card("2","H");
        Card c32 = new Card("3","D");
        Card c33 = new Card("5","S");
        Card c34 = new Card("9","C");
        Card c35 = new Card("K","D");
        Hand h7 = new Hand();
        List<Card> l7 = new ArrayList();l7.add(c31);l7.add(c32);l7.add(c33);l7.add(c34);l7.add(c35);
        h7.setCards(l7);
        h7.setName("Black");

        Card c36 = new Card("2","D");
        Card c37 = new Card("3","H");
        Card c38 = new Card("5","C");
        Card c39 = new Card("9","S");
        Card c40 = new Card("K","H");
        Hand h8 = new Hand();
        List<Card> l8 = new ArrayList();l8.add(c36);l8.add(c37);l8.add(c38);l8.add(c39);l8.add(c40);
        h8.setCards(l8);
        h8.setName("White");

        System.out.println("Test 4 : "+h7+"   "+h8);
        System.out.println("===> "+h7.compareToOtherHand(h8));

        Card c41 = new Card("2","H");
        Card c42 = new Card("2","D");
        Card c43 = new Card("2","S");
        Card c44 = new Card("2","C");
        Card c45 = new Card("K","D");
        Hand h9 = new Hand();
        List<Card> l9 = new ArrayList();l9.add(c41);l9.add(c42);l9.add(c43);l9.add(c44);l9.add(c45);
        h9.setCards(l9);
        h9.setName("Black");

        Card c46 = new Card("2","D");
        Card c47 = new Card("2","H");
        Card c48 = new Card("2","C");
        Card c49 = new Card("5","S");
        Card c50 = new Card("5","H");
        Hand h10 = new Hand();
        List<Card> l10 = new ArrayList();l10.add(c46);l10.add(c47);l10.add(c48);l10.add(c49);l10.add(c50);
        h10.setCards(l10);
        h10.setName("White");

        System.out.println("Test 5 : "+h9+"   "+h10);
        System.out.println("===> "+h9.compareToOtherHand(h10));

    }

}
