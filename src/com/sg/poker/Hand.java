package com.sg.poker;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Hand {

    private List<Card> cards;
    private String name;
    private String message;
    private Integer ranked;

    private List<Card> cardsInitiales;

    public void setCards(List<Card> cards) {
        this.cards = cards;
        this.ranked = 0;
        this.cardsInitiales= cards.stream().collect(Collectors.toList());
    }

    public List<Card> getCards(){
        return this.cards;
    }

    public void setMessageAndRanked() {

        if(getStraightFlush()!=null) {
            this.message = "Straight flush";
            this.ranked = 100+getStraightFlush().stream().mapToInt(Card::getNote).sum();
            this.cards.clear();
        }else
        if(getFoor()!=null) {
            this.message = "Four of a kind";
            this.ranked = 90+getFoor().get(0).getNote();
            removeByCard(getFoor().get(0).getValue());
        }else
        if(getFullHouse()!=null) {
            this.message = "Full House : "+getFullHouse().get(0)+" over "+getFullHouse().get(1);
            this.ranked = 80+getFullHouse().stream().mapToInt(Card::getNote).sum();
            String card1Toremove = getFullHouse().get(0).getValue();
            String card2Toremove = getFullHouse().get(1).getValue();
            removeByCard(card1Toremove);
            removeByCard(card2Toremove);
        }
        else
        if(getFlush()!=null) {
            this.message = "Flush";
            this.ranked = 70+getHighestCard().getNote();
            this.cards.clear();
        }
        else
        if(getStraight()!=null) {
            this.message = "Straight";
            this.ranked = 60+getStraight().getNote();
            removeByCard(getStraight().getValue());
        }
        else
        if(getThree()!=null) {
            this.message = "Three of a Kind";
            this.ranked = 50+getThree().get(0).getNote();
            removeByCard(getThree().get(0).getValue());
        }
        else
        if(getTwoPairs()!=null) {
            this.message = "Two Pairs";
            this.ranked = 40+getTwoPairs().stream().mapToInt(Card::getNote).sum();
            String pair1Toremove = getTwoPairs().get(0).getValue();
            String pair2Toremove = getTwoPairs().get(1).getValue();
            removeByCard(pair1Toremove);
            removeByCard(pair2Toremove);
        }
        else
        if(getPair()!=null) {
            this.message = "Pair";
            this.ranked = 30+getPair().get(0).getNote();
            removeByCard(getPair().get(0).getValue());
        }
        else
        if(getHighestCard()!=null) {
            this.message = "High Card: "+getHighestCard().getValue();
            this.ranked = 20+getHighestCard().getNote();
            removeByCard(getHighestCard().getValue());
        }

    }

    public String compareToOtherHand(Hand otherHand) {
        String m = "";
        while(m.isEmpty() && this.cards.size()>0) {

            setMessageAndRanked();
            otherHand.setMessageAndRanked();

            if( getRanked() > otherHand.getRanked()) {
                m = (getName()+" wins. - with "+getMassage());
            }
            if( getRanked() < otherHand.getRanked()) {
                m = (otherHand.getName()+" wins. - "+otherHand.getMassage());
            }

        }
        if(!m.isEmpty()) return m;
        return "Tie";
    }

    private Card getHighestCard() {
        Card card =this.cards.stream().max(Comparator.comparing(Card::getNote)).orElseGet(null);
        return card;
    }

    private List<Card> getPair(){
        List<Card> pairs = getAllDuplicatedOf(2);
        if(pairs==null || pairs.size()!=1) return null;
        return pairs;
    }

    private List<Card> getTwoPairs(){
        List<Card> twoPairs = getAllDuplicatedOf(2);
        if(twoPairs==null || twoPairs.size()!=2)return null;
        return twoPairs;
    }

    private List<Card> getThree() {
        return getAllDuplicatedOf(3);
    }

    private List<Card> getFoor() {
        return getAllDuplicatedOf(4);
    }


    private Card getStraight() {
        if (this.getCards().stream().distinct().count()!=5) return null;
        List<Card> sortedCards = this.cards.stream().sorted(Comparator.comparing(Card::getNote).reversed())
                .collect(Collectors.toList());
        for(int i = 0 ; i<sortedCards.size()-1;i++) {
            if(sortedCards.get(i+1).getNote()!= (sortedCards.get(i).getNote()-1) )
                return null;
        }
        return sortedCards.get(0);
    }

    private List<Card> getFlush() {
        if(this.getCards().size()!=5 | this.cards.stream().map(m->m.getType()).distinct().count()!=1)return null;
        return this.cards;
    }

    private List<Card> getFullHouse() {
        List<Card> pair = getPair();
        if(pair==null) return null;
        List<Card> threeOfKind = getThree();
        if(threeOfKind==null) return null;
        List<Card> full = new ArrayList();
        full.add(threeOfKind.get(0));
        full.add(pair.get(0));
        return full;
    }

    private List<Card> getStraightFlush() {
        Card straigth = getStraight();
        if(straigth==null) return null;
        List<Card> flush = getFlush();
        if(flush==null) return null;
        if(!straigth.getType().equals(flush.get(0).getType())) return null;
        return this.cards;
    }



    private List<Card> getAllDuplicatedOf(Integer number) {
        Map<String,List<Card>> mapCards = this.cards.stream()
                .collect(Collectors.groupingBy(p -> p.getValue()));
        if (mapCards.entrySet().size()>3)return null;
        List<Card> duplicatedCards= new ArrayList();
        mapCards.entrySet().stream().forEach(x-> {
            if(x.getValue().size()==number)
                duplicatedCards.add(x.getValue().get(0));
        });
        duplicatedCards.sort(Comparator.comparing(Card::getNote).reversed());
        return duplicatedCards.isEmpty()?null:duplicatedCards;
    }

    private Boolean isHightCardThen(Hand otherHand) {
        return this.getHighestCard().getNote() > otherHand.getHighestCard().getNote();
    }

    private void removeByCard(String value) {
        List<Card>  cards =  this.cards.stream().filter(f-> !f.getValue().equals(value)).collect(Collectors.toList());
        this.cards.clear();
        this.cards.addAll(cards);
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getRanked() {
        return this.ranked;
    }

    public String getMassage() {
        return this.message;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public String toString() {
        return name+": "+ cardsInitiales;
    }



}
